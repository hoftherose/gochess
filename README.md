GOCHESS

This is a simple chess game in go used to learn the functionalities of the language.

TODO LIST
- ~~Make simple chess board~~
- ~~Make chess pieces~~
- ~~Show chess board and pieces in ascii interface on shell~~
- ~~Accept input through shell interface to move pieces (illegal movements as well)~~
- Prevent illegal movements (only move on your turn, valid piece movement, not in check after moving)

NOT SURE LIST
- Prevent suicide (moving into check)
- Finding all available moves for testing with bots.
- Chess in interface
- Textbox move acceptance in interface
- Click and move pieces on interface
