package main

import (
	"fmt"
	"bufio"
	"os"

	Game "GoChess/game"
)

func main() {
	game := Game.Game{}
	game.InitializeGameBoard()
	scanner := bufio.NewScanner(os.Stdin)
	game.ShowBoard()
	for {
		fmt.Printf("Please enter movement: ")
		scanner.Scan()
		move := scanner.Text()
		err := game.Move(move)
		if err != nil{
			fmt.Println(err)
			continue
		}
		game.ShowBoard()
	}
	fmt.Println("Welcome to GoChess")
}