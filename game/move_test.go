package game

import "testing"

func TestMove(t *testing.T){
	game := Game{}
	game.InitializeGameBoard()
	var err error
	
	err = game.Move("2232")
	if game.Board[1][1] != Empty || game.Board[2][1] != WPawn{
		t.Errorf("Test 1 failed: Pawn movement didn't work as intended")
	} else if game.Turn == White{
		t.Errorf("Turn not swapped after moving")
	}
	err = game.Move("3222")
	if err == nil{
		t.Errorf("Can't move other player's space")
	} else if game.Turn == White{
		t.Errorf("Turn not maintained after failed movement")
	}
	err = game.Move("2232")
	if err == nil{
		t.Errorf("Can't move empty space")
	} else if game.Turn == White{
		t.Errorf("Turn not maintained after failed movement")
	}
	err = game.Move("8")
	if err == nil{
		t.Errorf("Can't move empty space")
	} else if game.Turn == White{
		t.Errorf("Turn not maintained after failed movement")
	}
}

func TestIncorrectInput(t *testing.T) {
	game := Game{}
	game.InitializeGameBoard()
	var err error
	err = game.Move("abcd")
	if err == nil{
		t.Errorf("Test fail: Can only enter numbers")
	}
	err = game.Move("123")
	if err == nil{
		t.Errorf("Test fail: Must enter 4 numbers")
	}
	err = game.Move("12345")
	if err == nil{
		t.Errorf("Test fail: Numbers must be from 1-8")
	}
	err = game.Move("9999")
	if err == nil{
		t.Errorf("Test fail: Numbers must be from 1-8")
	}
}