package utils

import "testing"

func TestMax(t *testing.T) {
	max := Max(1,3,4)
    if max != 4{
		t.Errorf("Wrong answer")
	}
}

func TestMin(t *testing.T) {
	min := Min(1,3,4)
    if min != 1{
		t.Errorf("Wrong answer")
	}
}