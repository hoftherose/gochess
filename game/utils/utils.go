package utils

import "math"

//Max get the min of various number
func Max(numbers ...int) int{
	len := len(numbers)
	if len < 2{
		return  0
	}
	currentMax := numbers[0]
	for i := 1; i < len; i++{
		currentMax = int(math.Max(float64(currentMax), float64(numbers[i])))
	}
	return currentMax
}

//Min get the min of various number
func Min(numbers ...int) int{
	len := len(numbers)
	if len < 2{
		return  0
	}
	currentMin := numbers[0]
	for i := 1; i < len; i++{
		currentMin = int(math.Min(float64(currentMin), float64(numbers[i])))
	}
	return currentMin
}

//Abs simple absolute value function for integers
func Abs(num int) int{
	if num < 0{
		return -num
	}
	return num
}