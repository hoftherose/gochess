package game

import (
	"fmt"
	"errors"
	"strconv"
	
	. "GoChess/game/pieces"
	"GoChess/game/utils"
)

func parseMovement(move string) (x1,y1,x2,y2 int, err error){
	//TODO add error handling
	if len(move)!=4{
		err = errors.New("Invalid input, must have exactly 4 characters")
		return 
	}
	num, err := strconv.ParseInt(move, 10, 64)
	y2 = int(num)%10-1
	x2 = int(num/10)%10-1
	y1 = int(num/100)%10-1
	x1 = int(num/1000)%10-1
	if utils.Max(x1,y1,x2,y2) >= 8 || utils.Min(x1,y1,x2,y2) < 0{
		err = errors.New("Invalid input, must have exactly 4 characters")
		return 
	}
	return
}

//Move piece from first coordinates (x1,y1) tp coordinates (x2,y2) where A-H is mapped to 1-8
func (game *Game) Move(move string) error {
	x1,y1,x2,y2, err := parseMovement(move)
	if err != nil{
		return err
	}
	err = game.ValidateMove(x1,y1,x2,y2)
	if err != nil{
		return err
	}
	game.Board[x2][y2] = game.Board[x1][y1]
	game.Board[x1][y1] = Empty
	game.Turn = -game.Turn
	return nil
}

//ValidateMove to make sure that a given move is permitted
func (game *Game) ValidateMove(x1,y1,x2,y2 int) error {
	piece := game.Board[x1][y1]
	destSquare := game.Board[x2][y2]
	if game.Turn*piece <= 0 || game.Turn*destSquare > 0{
		fmt.Printf("Please play fair, attempted to move %v into %v", piece, destSquare)
		return errors.New("Invalid Movement")
	}
	allowed := AllowedMovement(piece, x1,y1,x2,y2)
	if !allowed{
		return errors.New("Piece cannot move in that way, try again")
	}
	return nil
}