package game

import (
	"fmt"
	"testing"

	. "GoChess/game/pieces"
)

var startingBoard [8][8]int= [8][8]int{[8]int{WCastle, WHorse, WBishop, WKing, WQueen, WBishop, WHorse, WCastle},
								[8]int{WPawn, WPawn, WPawn, WPawn, WPawn, WPawn, WPawn, WPawn},
								[8]int{Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
								[8]int{Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
								[8]int{Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
								[8]int{Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
								[8]int{BPawn, BPawn, BPawn, BPawn, BPawn, BPawn, BPawn, BPawn},
								[8]int{BCastle, BHorse, BBishop, BKing, BQueen, BBishop, BHorse, BCastle}}

func TestGetBoard(t *testing.T) {
	game := Game{}
	fmt.Println(game)
	if board := game.Board; board != [8][8]int{}{
		t.Errorf("Board did not initialize correctly :O")
	}
}

func TestInitialState(t *testing.T) {
	game := Game{}
	game.InitializeGameBoard()
	if game.Board != startingBoard{
		t.Errorf("Failed board setup: Error initializing board")
	}
	if game.Turn != 1{
		t.Errorf("Failed board setup: Did not start with white")
	}
	if game.Winner != 0{
		t.Errorf("Failed board setup: Winner predetermined unfairly")
	}
}

func TestShowBoard(t *testing.T) {
	game := Game{}
	game.ShowBoard()
	game.InitializeGameBoard()
	game.ShowBoard()
}