package pieces

import (
	"fmt"
	"testing"
)

type MovementsTests struct {
	piece int
	x1 int
	y1 int
	x2 int
	y2 int
	result bool
}

func TestAllowedMovement(t *testing.T) {
	var tests = []MovementsTests{
        {WKing, 2,2,3,4, false},
        {BKing, 2,2,3,3, true},
        {WQueen, 1,1,3,3, true},
        {WHorse, 1,1,3,3, false},
        {BBishop, 1,1,3,3, true},
        {WPawn, 2,2,3,2, true},
        {BPawn, 2,2,3,2, false},
		{BHorse, 1,1,2,3, true},
    }
	for i, test := range tests{
		ans := AllowedMovement(test.piece, test.x1, test.y1, test.x2, test.y2)
		if ans != test.result{
			fmt.Printf("Failed on test %v, expected %v and received %v\n", i, test.result, ans)
			t.Errorf("Wrong Answer on AllowedMovement test")
		}
	}
}

func TestConfirmKingMove(t *testing.T) {
	var tests = []MovementsTests{
        {WKing, 2,2,3,4, false},
        {WKing, 2,2,3,3, true},
        {WKing, 2,2,2,4, false},
        {WKing, 2,2,3,2, true},
		{BKing, 1,1,2,7, false},
		{BKing, 7,7,1,1, false},
        {BKing, 3,3,2,3, true},
    }
	for i, test := range tests{
		ans := AllowedMovement(test.piece, test.x1, test.y1, test.x2, test.y2)
		if ans != test.result{
			fmt.Printf("Failed on test %v, expected %v and received %v\n", i, test.result, ans)
			t.Errorf("Wrong Answer on KingMovement test")
		}
	}
}

func TestConfirmCastleMove(t *testing.T) {
	var tests = []MovementsTests{
        {WCastle, 0,2,4,2, true},
        {WCastle, 2,2,3,3, false},
        {WCastle, 2,2,2,4, true},
        {WCastle, 2,2,3,4, false},
		{BCastle, 1,1,2,7, false},
		{BCastle, 1,1,1,7, true},
		{BCastle, 7,7,1,1, false},
        {BCastle, 0,0,8,0, true},
    }
	for i, test := range tests{
		ans := AllowedMovement(test.piece, test.x1, test.y1, test.x2, test.y2)
		if ans != test.result{
			fmt.Printf("Failed on test %v, expected %v and received %v\n", i, test.result, ans)
			t.Errorf("Wrong Answer on CastleMovement test")
		}
	}
}