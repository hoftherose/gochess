package pieces

import . "GoChess/game/utils"

//Square number mapping
const(
	BKing = iota-6
	BQueen
	BCastle
	BBishop
	BHorse
	BPawn
	Empty
	WPawn
	WHorse
	WBishop
	WCastle
	WQueen
	WKing
)

//CharMapping for printing out board in ascii in a more readable way (may change with ascii symbols)
var CharMapping = [13]string {"k", "q", "c", "b", "h", "p", ".", "P", "H", "B", "C", "Q", "K"}

//AllowedMovement checks to see if piece can move in a certain way.
func AllowedMovement(piece int, coor ...int) bool {
	switch Abs(piece) {
		case WKing:   return ConfirmKingMove(coor)
		case WQueen:  return ConfirmQueenMove(coor)
		case WCastle: return ConfirmCastleMove(coor)
		case WBishop: return ConfirmBishopMove(coor)
		case WHorse:  return ConfirmHorseMove(coor)
		case WPawn:   return ConfirmPawnMove(coor, piece)
	}
	return false
}


//ConfirmKingMove confirm move for king pieces
func ConfirmKingMove(coor []int) bool {
	return Abs(coor[0]-coor[2]) < 2 && Abs(coor[1]-coor[3]) < 2
}

//ConfirmQueenMove confirm move for queen pieces
func ConfirmQueenMove(coor []int) bool {
	return ConfirmCastleMove(coor) || ConfirmBishopMove(coor)
}

//ConfirmCastleMove confirm move for castle pieces
func ConfirmCastleMove(coor []int) bool {
	return coor[0] == coor[2] || coor[1] == coor[3]
}

//ConfirmBishopMove confirm move for bishop pieces
func ConfirmBishopMove(coor []int) bool {
	return Abs(coor[0]-coor[2]) == Abs(coor[1]-coor[3])
}

//ConfirmHorseMove confirm move for horse pieces
func ConfirmHorseMove(coor []int) bool {
	return (Abs(coor[0]-coor[2]) == 2 && Abs(coor[1]-coor[3]) == 1) || (Abs(coor[0]-coor[2]) == 1 && Abs(coor[1]-coor[3]) == 2)
}

//ConfirmPawnMove confirm move for pawn pieces
func ConfirmPawnMove(coor []int, color int) bool {
	if coor[0]+color == coor[2] && coor[1] == coor[3]{
		return true
	}
	if coor[0] == (7-5*color)/2{
		return coor[0]+2*color == coor[2] && coor[1] == coor[3]
	}
	return false
}