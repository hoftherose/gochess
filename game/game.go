package game

import . "GoChess/game/pieces"

//Constant interger values for defining players
const (
	Black = iota-1
	Tie
	White
)

//Game is a structure that contains the board, current turn and if a winner has been decided.
type Game struct {
	Board [8][8]int //TODO: change withSquare
	Turn int
	Winner int
}

//InitializeGameBoard only called to setup initial board state.
func (game *Game) InitializeGameBoard(){
	game.Turn = 1
	game.Board[7] = [8]int {BCastle, BHorse, BBishop, BKing, BQueen, BBishop, BHorse, BCastle}
	game.Board[6] = [8]int {BPawn, BPawn, BPawn, BPawn, BPawn, BPawn, BPawn, BPawn}
	game.Board[5] = [8]int {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty}
	game.Board[4] = [8]int {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty}
	game.Board[3] = [8]int {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty}
	game.Board[2] = [8]int {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty}
	game.Board[1] = [8]int {WPawn, WPawn, WPawn, WPawn, WPawn, WPawn, WPawn, WPawn}
	game.Board[0] = [8]int {WCastle, WHorse, WBishop, WKing, WQueen, WBishop, WHorse, WCastle}
}

//ShowBoard prints out a formatted version of the board for easy reading (defer to print rightside up)
func (game *Game) ShowBoard(){
	defer println("\n---------------")
	for _, row := range game.Board{
		line := ""
		for _, square := range row{
			line += CharMapping[square+6]+" "
		}
		defer println(line)
	}
	defer println()
}